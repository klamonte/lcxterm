LCXterm - Linux Console X-like Terminal
=======================================


WHAT IS LCXTERM?
----------------

LCXterm is a ncurses-based terminal emulator that brings additional
conveniences to the raw Linux console and other terminal emulators.
LCXterm is intended to act like an insertable component between the
user's normal terminal and a shell, and generally stay out of the way
otherwise until a feature is needed.

LCXterm can add the following features to your terminal:

* For raw Linux console, LCXterm can enable you to use GPM mouse
  through a network connection (ssh/telnet/etc), by converting GPM
  mouse events into X10 mouse protocol.  (This feature also works in
  "passthru" mode, see below.)

* Significantly faster processing and display.  LCXterm detects
  console floods and limits display refreshes, commonly resulting in
  10-20x speedup compared to raw Linux console and some X terminals.

* Arbitrarily long searchable scrollback, which can also be saved to
  file.  Bytes can be saved as UTF-8 text or HTML with color.  For the
  raw Linux console, this means having a scrollback that is not lost
  when switching virtual terminals.

* File transfers via the Xmodem, Ymodem, Zmodem, and Kermit protocols.
  (In passthru mode, uploads are not available, however Zmodem and
  Kermit downloads can autostart.)

* Session capture to file.  (This feature also works in passthru.)

* User-definable keyboard macros for function keys, arrow keys,
  Ins/Del/Home/End/PgUp/PgDn, etc.

* vttest-passing terminal emulation for VT100, VT102, VT220, Linux,
  and Xterm.

* The ability to spawn a local shell.

Since LCXterm uses ncurses for its display, it may lack features (such
as 24-bit RGB color) that you need.  Passthru mode is a compromise:
LCXterm sends data unaltered between the terminal and shell, so you
can use all of the features of your terminal, but still monitors the
stream so that it can provide session capture, GPM mouse conversion,
and Zmodem and Kermit autostart downloads.

The LCXterm homepage, which includes additional information and binary
release downloads, is at: https://lcxterm.sourceforge.io .  The
LCXterm source code is hosted at:
https://gitlab.com/AutumnMeowMeow/lcxterm .



SCREENSHOTS
-----------

Available commands in normal/non-passthru mode are shown below.  One
can press Alt-Z to see this list.

![Normal command menu](/misc/screenshots/command_menu.png?raw=true "Normal command menu")

Demonstration of using the persistent scrollback, and saving a bit of
it to file.

![Demonstration of scrollback](/misc/screencaps/scrollback_ttyrecord.gif?raw=true "Demonstration of scrollback")

An example of passthru mode.  xterm is running in full 256-color
glory.  LCXterm picks up the Zmodem autostart command, completes
the download, and resumes passthru mode after.

![Passthru mode showing Zmodem autostart on 256-color xterm](/misc/screencaps/xterm_zmodem_ttyrecord.gif?raw=true "Passthru mode showing Zmodem autostart on 256-color xterm")

Some vttest screens.  This is not passthru mode, LCXterm uses a trick
to send the double-height/double-width control sequences to xterm even
when using ncurses.

![vttest screens](/misc/screencaps/vttest_ttyrecord.gif?raw=true "vttest screens")


COPYRIGHT STATUS
----------------

To the extent possible under law, the author(s) of LCXterm have
dedicated all copyright and related and neighboring rights to LCXterm to
the public domain worldwide. This software is distributed without any
warranty.  The COPYING file describes this intent, and provides a
public license fallback for those jurisdictions that do not recognize
the public domain.

Anyone is free to see and modify the source code and release new
versions under whatever license terms they wish.

LCXterm includes a UTF-8 decoding function in codepage.c, which is
available under the MIT license.  The combined license terms for the
complete LCXterm executable is effectively the same as the MIT
license: attribution in the source, source is not required to be
shared, and there is no warranty.



BUILDING LCXTERM
----------------

LCXterm can be built in several ways:

* An autoconf build is available: use `configure ; make` .

* A very simple barebones build is also provided via `make -f
  build/Makefile.generic`.

* A Debian packages for `lcxterm` is available in the build/deb
  directory.

* An RPM spec file for `lcxterm` is available in the build/rpm
  directory.

The INSTALL file has some additional details.



HOW TO USE LCXTERM
------------------

LCXterm is driven by the keyboard.  It will listen for mouse events,
but only to send those to remote systems using the XTERM (default) or
X_8BIT emulations.

LCXterm normally exits when the shell exits.  It can be exited
explicitly by the Alt-H kill shell command.

Nearly all of the time pressing the ESCAPE key or the backtick (`)
will exit a dialog.

Use 'lcxterm -p' or 'lcxterm --passthru' to use passthru mode.

Press Alt-Z to see the commands menu.



TERMINAL EMULATION LIMITATIONS
------------------------------

This section describes known missing features in a LCXterm emulation.

----

XTERM (and X_8BIT) recognizes only a few more features than LINUX and
VT220.  It does not support most of the advanced features unique to
XTerm such as Tektronix 4014 mode, alternate screen buffer, and many
more.  It is intended to support XTerm applications that only use the
sequences in the 'xterm' terminfo entry.

24-bit RGB color is not supported, due to lack of support in the
ncurses API.

The following features are not supported in the VT100/VT220 feature
set: smooth scrolling, printing, keyboard locking, tests, user-defined
keys (DECUDK), downloadable fonts (DECDLD), VT100/ANSI compatibility
mode (DECSCL).  (Also, because the VT220/XTERM emulation does not
support DEC VT100/ANSI mode, it will fail the last part of the vttest
"Test of VT52 mode".)  The unsupported commands are parsed to keep a
clean display, but not used otherwise.

132-column mode is supported only within consoles/emulators that have
132 (or more) columns available.  For instance, 132-column output on a
128-column Linux console screen will result in incorrect behavior.

Numeric/application keypad modes do not work well.  This is due to the
host console translating the numeric keypad keys on its own before
sending the keystroke to the (n)curses library.  For example, the
Linux console will transmit the code corresponding to KEY_END when the
number pad "1 key" is pressed if NUMLOCK is off; if NUMLOCK is on the
console will transmit a "1" when the "1 key" is pressed.  LCXterm thus
never actually sees the curses KEY_C1 code that would instruct LCXterm
to transmit the appropriate string to the host system.  The only key
that appears to work right on most consoles is the number pad "5 key"
(KEY_B2).

VT52 HOLD SCREEN mode is not supported.

In VT52 graphics mode, the 3/, 5/, and 7/ characters (fraction
numerators) are not rendered correctly.

All data meant for the 'printer' (CSI Pc ? i) is quietly discarded.



FILE TRANSFER PROTOCOL LIMITATIONS
----------------------------------

This section describes known issues in the LCXterm serial file
transfer protocols.

----

ASCII uploads may hang if the remote end can't keep up.  For instance,
using 'vi' to create a large file and ASCII uploading the contents may
hang after a few kilobytes.  'cat > filename' usually works fine.

ASCII downloads will process the TAB character (0x09) as a control
character, causing it to expand to the appropriate number of spaces.

On Ymodem downloads, if the file exists it will be appended to.

Xmodem and Ymodem downloads from hosts that use the rzsz package might
need to have stderr redirected to work correctly, for example 'sb
filename 2>/dev/null' .

Kermit receive mode by default handles file collisions by saving to a
new file (SET FILE COLLISION RENAME / WARN file access Attribute).  It
supports the APPEND file access Attribute but disregards the SUPERSEDE
file access Attribute.

When sending files via Zmodem to HyperTerminal, if the HyperTerminal
user clicks "Skip file" then the transfer will stall.  This appears to
be due to two separate bugs in HyperTerminal: 1) When the user clicks
"Skip File", HyperTerminal sends a ZRPOS with position==file size,
LCXterm responds by terminating the data subpacket with ZCRCW, which
HyperTerminal responds to with ZACK, however the ZACK contains an
invalid file position.  2) LCXterm ignores bug #1 and sends ZEOF, to
which HyperTerminal is supposed to respond with ZRINIT, however
HyperTerminal hangs presumably because it is expecting the ZEOF to
contain a particular file position, however the position it desires is
neither the true file size nor the value it returned in the ZACK.



TROUBLESHOOTING / WORKAROUNDS
-----------------------------

Sometimes LCXterm can't behave like a real hardware or X11-based
terminal.  This section describes those things, and some of the
workarounds that might help.

----

LCXterm requires a Unicode-capable Linux console or X emulator to look
right.  For the Linux console, the default settings for most Linux
distributions should work well.  Under X11, xterm, rxvt-unicode, and
Konsole work well.

The backspace key is always mapped to DEL (0x7F) in VT220 emulation to
match the keyboard of a real VT220.  You can send a true backspace
(0x08, ^H) by pressing Alt-\ 0 0 8 to use the Alt Code Key feature to
send backspace.

Function keys beyond F4 in VT100/VT102 emulation may not work as
expected.  LCXterm uses a common convention that F5 is "{ESC} O t", F6
is "{ESC} O u", etc.  Some programs understand this convention.  Those
that don't will usually understand "{ESC} {number}", where {number} is
a number from 5 to 0, to mean F5 through F10.  You can get this effect
in LCXterm by typing ESC {number}, or by switching to Doorway Mode and
typing Alt-{number} (or Meta-{number}).

In VT100, VT102, and LINUX emulations, some programs (like minicom and
Midnight Commander) send the DECCOLM sequence ({ESC} [ ? 3 l ) when
exiting, putting the emulation into 80-column mode.  Resetting the
emulation via Alt-G {pick emulation} {enter 'y'} will restore the
default right margin.

Malformed escape sequences might "freeze" the emulation.  (For
example, receiving a 0x90 character causes VT102 to look for a DCS
sequence.  If the DCS sequence is not properly terminated the
emulation won't recover.)  Resetting the current emulation will
restore the console function.

KEY_SUSPEND is usually mapped to Ctrl-Z and used to suspend the local
program ('lcxterm').  If LCXterm sees KEY_SUSPEND it will assume the
user typed Ctrl-Z and meant to pass that to the remote side.



DOCUMENTATION
-------------

LCXterm has two sources of documentation:

* This README.

* The lcxterm man page.



OTHER PROJECTS
--------------

Users of LCXterm might be interested in these other projects:

* Qodem Terminal Emulator: https://qodem.sourceforge.net and
  https://github.com/AutumnMeowMeow/qodem .  LCXterm's direct
  ancestor, a full-featured re-implementation of the DOS-era
  Qmodem(tm) communications program.

* Jexer - Java Text User Interface library:
  https://gitlab.com/AutumnMeowMeow/jexer .  A Java-based lookalike of
  the Turbo Vision library.

* Jermit - Java Serial File Transfer Protocols:
  https://gitlab.com/AutumnMeowMeow/jermit .  A Java-based
  re-implementation of Xmodem, Ymodem, Zmodem, and Kermit.

* The Kermit Project: http://www.kermitproject.org/ .  The Swiss army
  knife of serial file transfer protocols.
