.TH lcxterm 1 "June 12, 2020"

.SH NAME
lcxterm \- Linux Console X-like Terminal

.SH SYNOPSIS
.ll +8
.B lcxterm
.RI "[ \-\-dotlcxterm\-dir DIRNAME ]"
.in 15
.br
.RI "[ \-\-config FILENAME ]"
.br
.RI "[ \-\-create\-config FILENAME ]"
.br
.RI "[ \-\-capfile FILENAME ]"
.br
.RI "[ \-\-keyfile FILENAME ]"
.br
.RI "[ \-p, \-\-passthru ]"
.br
.RI "[ \-\-read\-only ]"
.br
.RI "[ \-\-doorway MODE ]"
.br
.RI "[ \-\-codepage CODEPAGE ]"
.br
.RI "[ \-\-emulation EMULATION ]"
.br
.RI "[ \-\-status\-line { on | off } ]"
.br
.ll -8

.TP
.BR lcxterm " \-\-version"

.TP
.BR lcxterm " [ \-\-help | \-h | \-? ]"

.SH DESCRIPTION
.I LCXterm
is a ncurses\-based terminal emulator that brings additional
conveniences to the raw Linux console and other terminal emulators.
LCXterm is intended to act like an insertable component between the
normal terminal and a shell, and generally stay out of the way
otherwise until a feature is needed.

Features that work in "passthru" mode include:

    * Zmodem and Kermit downloads.

    * GPM to X10 mouse conversion for the Linux console.  This enables
      Linux console mouse events to work across remote shells.

    * Session capture to file.

When used in "non\-passthru" mode, LCXterm provides all of the above
plus:

    * Xmodem, Ymodem, Zmodem, and Kermit uploads and downloads.

    * Arbitrarily long searchable scrollback, which can also be saved
      to file.  Bytes can be saved as UTF-8 text or HTML with color.

    * User\-definable keyboard macros for function keys, arrow keys,
      Ins/Del/Home/End/PgUp/PgDn, etc.

    * vttest\-passing terminal emulation for VT100, VT102, VT220,
      Linux, and Xterm.

    * Significantly faster processing and display.

.SH OPTIONS
.TP
.B \-\-dotlcxterm\-dir DIRNAME
Use DIRNAME instead of $HOME/.local/share/lcxterm for config/data
files.
.TP
.B \-\-config FILENAME
Load options from FILENAME (only).
.TP
.B \-\-create\-config FILENAME
Write a new options file to FILENAME and exit.
.TP
.B \-\-capfile FILENAME
Capture the entire session and save to FILENAME.
.TP
.B \-\-keyfile FILENAME
Load keyboard macros from FILENAME.
.TP
.B \-p, \-\-passthru
Pass screen and keyboard data directly between the host terminal and
remote, initializing ncurses (temporarily) only for Zmodem/Kermit
autostart downloads.  GPM mouse and session capture will work in
passthru mode; keyboard macros and file uploads will not.
.TP
.B \-\-read\-only
Disable all writes to disk.  No config files will be saved, downloads
are disabled, scripts are disabled, no screen dump, capture, logging,
etc.
.TP
.B \-\-doorway MODE
Select doorway MODE.  Valid values for MODE are "doorway", "mixed",
and "off".
.TP
.B \-\-codepage CODEPAGE
Select codepage CODEPAGE.  See Alt\-; list for valid codepages.
Example: "CP437", "CP850", "Windows\-1252", etc.
.TP
.B \-\-emulation EMULATION
Select emulation EMULATION.  Valid values are "vt100", "vt102",
"vt220", "l_8bit", "linux", "x_8bit", and "xterm".
.TP
.B \-\-status\-line { on | off }
If "on" enable status line.  If "off" disable status line.
.TP
.BR \-\-version
Display program version and brief public domain dedication statement.
.TP
.BR \-\-help ", " \-h ", " \-?
Display usage screen.

.SH TERMINAL REQUIREMENTS

LCXterm requires the following characteristics of its shell/terminal:

  * Unicode capability.  The raw Linux console, UXTerm, URxvt,
    Konsole, and gnome\-terminal all work well.

  * A Unicode font that includes the CP437 (PC VGA) glyphs.

  * The meta key must use escape prefixing rather than setting the 8th
    bit on the character.  Many terminal emulators do this by
    default.  LCXterm sends an xterm escape sequence to enable this
    behavior.  For XTerm, this can be enabled by clicking
    CTRL\-Mouse1 (left button) and checking "Meta Sends Escape", or by
    adding the following lines to ~/.Xresources:
.br
        XTerm*metaSendsEscape:   true
.br
        UXTerm*metaSendsEscape:  true

.SH EMULATIONS

Emulations supported by LCXterm and their status follows.  Note that
LINUX and XTERM emulations can be set to CP437 (DOS VGA),
ISO\-8859\-1, CP720 (DOS Arabic), CP737 (DOS Greek), CP775 (DOS Baltic
Rim), CP850 (DOS West European), CP852 (DOS Central European), CP857
(DOS Turkish), CP858 (DOS West European+Euro), CP860 (DOS Portuguese),
CP862 (DOS Hebrew), CP863 (DOS Quebecois), CP866 (DOS Cyrillic),
Windows\-1250 (Central/East European), Windows\-1251 (Cyrillic),
Windows\-1252 (West European), KOI8\-R (Russian), and KOI8\-U
(Ukrainian).

When 8\-bit characters are used (see Alt\-; Codepage below), the C0
control characters (0x00 \- 0x1F, 0x7F) are mapped to the equivalent
CP437 glyphs when there are no glyphs defined in that range for the
set codepage.

.TP
.B
VT100
Identical to VT102 except in how it responds to Device Attributes
function.
.TP
.B
VT102
Fairly complete.  Does not support smooth scrolling, printing,
keyboard locking, and hardware tests.  Many numeric keypad characters
also do not work correctly due to console NUM LOCK handling.
132\-column output is only supported if the host console / window is
already 132 columns or wider; LCXterm does
.B not
issue resize commands to the host console for 80/132 column switching
codes.
.TP
.B
VT220
Fairly complete.  Converts National Replacement Character sets and DEC
Supplemental Graphics characters to Unicode.  In addition to
limitations of VT102, also the following features are not supported:
user\-defined keys (DECUDK), downloadable fonts (DECDLD), VT100/ANSI
compatibility mode (DECSCL).
.TP
.B
LINUX
This emulation has two modes: PC VGA (L_8BIT) and UTF\-8 (LINUX).
This emulation is similar to VT102 but also recognizes the Linux
private mode sequences and ECMA\-48 sequences VPA, CNL, CPL, ECH, CHA,
VPA, VPR, and HPA.  In addition to VT102 limitations, also the
following features are not supported: selecting ISO 646/ISO
8859\-1/UTF\-8 character sets, X11 mouse reporting, and setting the
scroll/caps/numlock leds.
.TP
.B
XTERM
This emulation has two modes: PC VGA (X_8BIT) and UTF\-8 (XTERM).  It
recognizes everything in LINUX, VT220, and a few more XTerm sequences.
It does not support most of the advanced features unique to XTerm such
as Tektronix 4014 mode, alternate screen buffer, and many more.  It is
intended to support XTerm applications that only use the sequences in
the 'xterm' terminfo entry.

.SH "FILE TRANSFER PROTOCOLS"
Protocols supported by LCXterm and their status follows:
.TP
.B
XMODEM
Supports original Xmodem, Xmodem\-1k, Xmodem\-CRC16, and Xmodem\-G.  Also
supports "Xmodem Relaxed", a variant of original Xmodem with longer
timeouts.
.TP
.B
YMODEM
Supports Ymodem and Ymodem\-G.  If a file exists, it will be appended to.
.TP
.B
ZMODEM
Supports resume (crash recovery) and auto\-start.  Does not yet support
changing block size on errors, so transfers over unreliable
serial/modem connections might incur significant performance
penalties.
.TP
.B
KERMIT
Supports the original robust (slow) Kermit plus streaming, sliding
windows, and autostart.  On reliable connections with streaming it
should perform reasonably well.  Does not yet support long or
extra\-long packets, RESEND/REGET, or server mode.

.SH INTERFACE

.B Terminal Mode
.br
Terminal Mode is the main LCXterm terminal emulator interface.
.B Alt\-Z
will bring up a help menu.  While in Terminal Mode the following
actions are supported:
.TP
.B Alt\-G Term Emulation
This brings up a menu to select the terminal emulation.  Selecting the
active terminal emulation will prompt to reset the emulation state;
this may be useful to recover from corrupted escape sequences.
.TP
.B Alt\-C Clear Screen
This clears the screen and homes the cursor.
.TP
.B Alt\-P Capture File
Enable/disable capture to file.  Four capture formats are supported:
"raw", "normal", "html", and "ask".  "Raw" format saves every byte as
received from the other side before emulation processing; "normal"
saves UTF\-8 characters after emulation processing; "html" saves in
HTML format with Unicode entities and color attributes after emulation
processing; "ask" will bring up a dialog to select which format to use
every time capture is enabled.  ASCII file transfers will be included
in the capture file; other file transfers (Xmodem, Ymodem, Zmodem,
Kermit) are excluded from the capture file.
.TP
.B Alt\-S Split Screen
This actives split screen mode, in which local characters are
accumulated in a buffer before sending to the remote side.  To send
carriage return, enter "^M".
.TP
.B Alt\-T Screen Dump
This prompts for a filename, and then saves the current view to that
file.  Three screen dump formats are supported: "normal", "html", and
"ask".  "normal" saves UTF\-8 characters after emulation processing;
"html" saves in HTML format with Unicode entities and color attributes
after emulation processing; "ask" will bring up a dialog to select
which format to use every time the screen is dumped.
.TP
.B PgUp Upload Files
This brings up the file upload menu.  Note that CTRL\-PgUp and ALT\-PgUp
may also work depending on the terminfo for the host terminal.
.TP
.B PgDn Download Files
This brings up the file download menu.  Note that CTRL\-PgDn and
ALT\-PgDn may also work depending on the terminfo for the host
terminal.
.TP
.B Alt\-\\\\ Alt Code Key
This brings up a dialog to enter the 3\-digit decimal value (0\-255) for
an 8\-bit byte or a 4\-digit hexadecimal value (0\-ffff) for a 16\-bit
Unicode character (LINUX and XTERM only).
.TP
.B Alt\-; Codepage
This brings up a dialog to change the current codepage.  Codepages are
limited by the current emulation.  VT52, VT100, VT102, VT220, LINUX,
and XTERM have their own codepage; L_8BIT and X_8BIT emulations can be
set to: CP437 (VGA), ISO\-8859\-1, CP720 (Arabic), CP737 (Greek),
CP775 (Baltic Rim), CP850 (Western European), CP852 (Central
European), CP857 (Turkish), CP858 (Western European with euro), CP860
(Portuguese), CP862 (Hebrew), CP863 (Quebec French), CP866 (Cyrillic),
CP1250 (Central/Eastern European), CP1251 (Cyrillic), CP1252 (Western
European), KOI8_R (Russian),and KOI8_U (Ukrainian).
.TP
.B Alt\-/ Scroll Back
This selects the scrollback buffer.  When viewing the buffer, "S"
saves to file and "C" clears the scrollback buffer.  By default LCXterm
supports up to 20000 lines of scrollback; this can be changed by
editing scrollback_max in scrollback.c.  Three scrollback save formats
are supported: "normal", "html", and "ask".  "normal" saves UTF\-8
characters after emulation processing; "html" saves in HTML format
with Unicode entities and color attributes after emulation processing;
"ask" will bring up a dialog to select which format to use every time
the scrollback is saved.
.TP
.B Alt\-H Kill Shell
This kills the shell child process.
.TP
.B Alt\-J Function Keys
This brings up the keyboard macro editor.  Keyboard macros support
substitutions for control characters including carriage return ("^M"),
the phone book entry username ("$USERNAME"), and the phone book entry
password ("$PASSWORD").
.TP
.B Alt\-N Configuration
This brings the
.B lcxtermrc
options file up in an editor.
.TP
.B Alt\-R OS Shell
This spawns a system shell.
.TP
.B Alt\-V View File
This brings up a prompt to view a file in an editor.
.TP
.B Alt\-W List Directory
This brings up a directory listing.
.TP
.B Alt\-7 Status Line Info
This selects between two formats for the status line.
.TP
.B Alt\-8 Hi\-Bit Strip
This selects whether or not to clear the 8th bit of all incoming
bytes.  Note that high\-bit stripping occurs before both emulation
processing and UTF\-8 decoding.
.TP
.B Alt\-B Beeps & Bells
This toggles beep support on or off.  When beep support is on, beeps
from the remote host will be played by LCXterm.
.TP
.B Alt\-E Half/Full Duplex
This toggles between half and full duplex.
.TP
.B Alt\-U Scrollback Record
This selects whether or not lines that scroll off the top of the
screen will be saved to the scrollback buffer.
.TP
.B Alt\-= Doorway Mode
This selects between three doorway modes: "Doorway OFF", "Doorway
MIXED" and "Doorway FULL".  When doorway mode is "Doorway OFF",
terminal mode responds to all of its command keys as described in this
section.  When doorway mode is "Doorway FULL", all Alt\- command
keystrokes except
.B Alt\-=
are passed to the remote side.  When doorway mode is "Doorway MIXED",
terminal mode supports a few commands but passes the majority of Alt\-
command keystrokes to the remote side.  The default commands supported
in "Doorway MIXED" mode are:
     Alt\-P Capture
     Alt\-T Screen Dump
     Alt\-Z Menu
     Alt\-/ Scrollback
     Alt\-PgUp or Ctrl\-PgUp Upload Files
     Alt\-PgDn or Ctrl\-PgDn Download Files
.TP
.B Alt\-\- Status Lines
This toggles the status line on or off.
.TP
.B Alt\-+ CR/CRLF Mode
This toggles whether or not received carriage returns imply line feed
or not.

.SH ENVIRONMENT VARIABLES
.TP
LCXterm makes use of the following variables:
.TP
.B HOME
The user's home directory.  LCXterm creates two directories:
.BR "$HOME/.lcxterm" " for keyboard macro files, and " "$HOME/lcxterm" " for uploaded and downloaded files, screen dumps, capture, etc."
.TP
.B EDITOR
If present, this will be used when spawning external editors (log
view, edit configuration, view file, edit phone book note).  If not
present, by default (editable in the options file) use
.BR vi "."
.TP
.B ESCDELAY
This is a timeout value in milliseconds used by the ncurses
.BR get_wch() " and " wget_wch()
functions to determine if a user pressed bare ESCAPE.  On some systems
pressing ESCAPE may require up to a full second before LCXterm can
process it.  For LCXterm backtick (`) can be used instead of ESCAPE.

.SH FILES
.TP
.B $HOME/.lcxterm/lcxtermrc
LCXterm options/configuration file.  Inline comments describe the options.

.SH SEE ALSO
.BR "kermit"(1), " gkermit"(1), " minicom"(1), " xterm"(1), " vttest"(1), " console\-codes"(4)

.SH HOMEPAGE
.PP
The LCXterm code repository is hosted at
<https://gitlab.com/AutumnMeowMeow/lcxterm>.

.SH PUBLIC DOMAIN NOTICE
Written 2003\-2021 by Autumn Lamonte
.PP
To the extent possible under law, the author(s) have dedicated all
copyright and related and neighboring rights to this software to the
public domain worldwide. This software is distributed without any
warranty.
