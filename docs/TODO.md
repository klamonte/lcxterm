lcxterm TODO List
=================

1.0

  Autotrigger macros
    No UI, configure file only
    Use true regexp
      PCRE (pcre.h) or POSIX (regex.h)?
    Actions:
      Take screen dump
      Save all scrollback
      Start/stop capture
      Kill shell
      Respond with (include matches)
      Upload file (include matches)
      Download file (include matches)
      Spawn command (include matches, wait or just launch in background)


Regression Checklist
--------------------

  Retrofit fixes to Qodem

  Persistence
    Keyboard editor
    Edit options
      Reload options after edit

  Online features
√   Codepage
    Keyboard macros
    Split screen
    Scrollback
      View up/down
      Clear
      Save to file
        Normal
        Html
    Screen dump
      Normal
      Html
    List directory
    View file
√   Operating system shell
    Capture
      Raw
      Normal
      Html

  UI Conventions
    F10 -> Enter or Alt-Enter
    F2 -> Space

  Toggles
    Full duplex
    Strip 8th
    Beeps and bells
    Hard backspace
    Linewrap
    Display NUL
    Scrollback
    Status line visible
    Status line info
    Add CR

  Alt Code key
    8-bit
    Unicode

  Uploads
    ASCII
    Xmodem
    Xmodem CRC
    Xmodem 1k
    Xmodem Relaxed
    Xmodem 1k-G
    Ymodem
    Ymodem G
√   Zmodem
    Kermit

  Downloads
    ASCII
    Xmodem
    Xmodem CRC
    Xmodem 1k
    Xmodem Relaxed
    Xmodem 1k-G
    Ymodem
    Ymodem G
√   Zmodem
    Kermit

  Emulations:
    VT100
    VT102
    VT220
    LINUX
    XTERM
    L_8BIT
    X_8BIT

  Command-line switches:
    --dotlcxterm-dir
    --config
    --create-config
    --capfile
    --keyfile
    --read-only
    --doorway
    --codepage
    --emulation
    --status-line
    --version
    -h, --help



Release Checklist
-----------------

Fix all marked TODOs in code

√ Eliminate DEBUG_*, // comments

√ Update ChangeLog

√ #define RELEASE common.h

√ Version in:
    configure.ac
    common.h
    main.c
    build/deb/lcxterm/changelog
    build/deb/lcxterm/control
    build/rpm/lcxterm.spec

√ Update written by date to current year:
    All code headers
    main.c --version

Verify .tar.gz source builds cleanly

Tag gitlab


1.1.0 Wishlist
--------------




Brainstorm Wishlist
-------------------

Multi-line customizable status display




Bugs Noted In Other Programs
----------------------------
