/*
 * shell.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "qcurses.h"
#include "common.h"

/* Find the right header for forkpty() */
#  ifdef __APPLE__
#    include <util.h>
#  else
#    if defined(__FreeBSD__) || \
        defined(__OpenBSD__) || \
        defined(__NetBSD__)
#      include <sys/types.h>
#      include <util.h>
#    else
#      include <pty.h>
#    endif
#  endif
#  include <termios.h>
#  include <unistd.h>

#include <sys/ioctl.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <assert.h>
#include <fcntl.h>
#include "main.h"
#include "states.h"
#include "screen.h"
#include "options.h"
#include "shell.h"

/*
 * POSIX case: we need to reset the child exited flag before forkpty().
 */
extern Q_BOOL q_child_exited;

/**
 * Set a tty into raw mode.
 *
 * @param tty_fd the tty descriptor
 */
void set_raw_termios(const int tty_fd) {
    struct termios old_termios;
    struct termios new_termios;

    if (tcgetattr(tty_fd, &old_termios) < 0) {
        /*
         * Error, bail out.
         */
        return;
    }
    memcpy(&new_termios, &old_termios, sizeof(struct termios));
    cfmakeraw(&new_termios);
    if (tcsetattr(tty_fd, TCSANOW, &new_termios) < 0) {
        /*
         * Error, bail out.
         */
        return;
    }

    /*
     * All OK
     */
    return;
}

/**
 * Convert a command line string with spaces into a NULL-terminated array of
 * strings appropriate to passing to the execvp().
 *
 * @param argv the command line string
 * @return the array
 */
static char ** tokenize_command(const char * argv) {
    int i, j;
    int argv_start;
    char ** new_argv;
    char * old_argv;
    int n;

    old_argv = Xstrdup(argv, __FILE__, __LINE__);
    n = strlen(argv);

    /*
     * Trim beginning whitespace
     */
    argv_start = 0;
    while (q_isspace(argv[argv_start])) {
        argv_start++;
    }

    for (i = 0, j = argv_start; j < n; i++) {
        while ((j < n) && !q_isspace(argv[j])) {
            j++;
        }
        while ((j < n) && q_isspace(argv[j])) {
            j++;
        }
    }

    /*
     * i is the number of tokens.
     */
    new_argv = (char **) Xmalloc(sizeof(char *) * (i + 1), __FILE__, __LINE__);
    for (i = 0, j = argv_start; j < n; i++) {
        new_argv[i] = old_argv + j;
        while ((j < n) && !q_isspace(old_argv[j])) {
            j++;
        }
        while ((j < n) && q_isspace(old_argv[j])) {
            old_argv[j] = 0;
            j++;
        }
    }

    /*
     * Why can't I free this?  Because new_argv is pointers into old_argv.
     * Freeing it kills the child process.
     */
    /*
     * Xfree(old_argv, __FILE__, __LINE__);
     */

    new_argv[i] = NULL;
    return new_argv;
}

/**
 * Construct the appropriate command line for a shell.
 *
 * @return the command line
 */
static char * connect_command() {
    char connect_string[COMMAND_LINE_SIZE];
    snprintf(connect_string, sizeof(connect_string), "%s",
        get_option(Q_OPTION_SHELL));
    return Xstrdup(connect_string, __FILE__, __LINE__);
}

/**
 * Spawn a sub-process.  NOTE: ANY CHANGES TO BEHAVIOR HERE MUST BE CHECKED
 * IN script_start() ALSO!!!
 *
 * @param command_line the command line.  Note that it will be free()'d in
 * this function.
 * @param emulation Q_EMUL_TTY, Q_EMUL_ANSI, etc.
 */
static void spawn_process(char * command_line, Q_EMULATION emulation) {

    /*
     * POSIX case: fork and execute
     */

    /*
     * Assume the child has not yet exited.
     */
    q_child_exited = Q_FALSE;

    /*
     * Fork and put the child on a new tty
     */
    char ** target_argv;
    char ttyname_buffer[FILENAME_SIZE];
    pid_t child_pid = forkpty(&q_child_tty_fd, ttyname_buffer, NULL, NULL);
    q_child_ttyname = Xstrdup(ttyname_buffer, __FILE__, __LINE__);

    if (child_pid == 0) {
        /*
         * Child process, will become the connection program.
         */

        struct q_scrolline_struct * line;
        struct q_scrolline_struct * line_next;

        /*
         * This just has to be long enough for LINES=blah and COLUMNS=blah
         */
        char buffer[32];
        int columns;

        /*
         * Restore signal handlers
         */
        signal(SIGPIPE, SIG_DFL);

        /*
         * Free scrollback memory
         */
        line = q_scrollback_buffer;
        while (line != NULL) {
            line_next = line->next;
            Xfree(line, __FILE__, __LINE__);
            line = line_next;
        }

        /*
         * Set TERM, LANG, LINES, and COLUMNS.
         */
        if (strlen(emulation_term(q_status.emulation)) == 0) {
            unsetenv("TERM");
        } else {
            setenv("TERM", emulation_term(q_status.emulation), 1);
        }
        snprintf(buffer, sizeof(buffer), "LANG=%s",
                 emulation_lang(q_status.emulation));
        putenv(strdup(buffer));

        memset(buffer, 0, sizeof(buffer));
        snprintf(buffer, sizeof(buffer), "LINES=%u", HEIGHT - STATUS_HEIGHT);
        putenv(strdup(buffer));
        memset(buffer, 0, sizeof(buffer));

        columns = WIDTH;
        snprintf(buffer, sizeof(buffer), "COLUMNS=%u", columns);
        putenv(strdup(buffer));

        /*
         * Set the TTY cols and rows.  This handles the case for those
         * programs that don't propogate LINES and COLUMNS.
         *
         * I use perror() here because it will make its way back to the
         * parent (I hope).  This child process doesn't have control of the
         * terminal anymore so I can't use ncurses functions.
         */
        struct winsize console_size;
        if (ioctl(STDIN_FILENO, TIOCGWINSZ, &console_size) < 0) {
            perror("ioctl(TIOCGWINSZ)");
        } else {
            console_size.ws_row = HEIGHT - STATUS_HEIGHT;
            console_size.ws_col = columns;
            if (ioctl(STDIN_FILENO, TIOCSWINSZ, &console_size) < 0) {
                perror("ioctl(TIOCSWINSZ)");
            }
        }

        /*
         * Separate target into arguments.
         */
        target_argv = tokenize_command(command_line);
        Xfree(command_line, __FILE__, __LINE__);
        execvp(target_argv[0], target_argv);
        /*
         * If we got here, then we failed to spawn.  Emit the error message
         * and crap out.
         */
        perror("execvp()");
        exit(-1);
    } else {

        /*
         * Free leak
         */
        Xfree(command_line, __FILE__, __LINE__);

    } /* if (child_pid == 0) */

    if (q_child_tty_fd != -1) {
        q_child_pid = child_pid;
    }
}

/**
 * Set a file descriptor or Winsock socket handle to non-blocking mode.
 *
 * @param fd the descriptor
 */
static void set_nonblock(const int fd) {
    int flags;

    flags = fcntl(fd, F_GETFL);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

/**
 * Spawn a local shell in its own pty.
 */
void launch_shell() {
    char * command;

    /*
     * Assert that the connection does not already exist.
     */
    assert(q_child_tty_fd == -1);

    /*
     * Reset the terminal emulation state.
     */
    reset_emulation();

    /*
     * Push all the data out to clear the soon-to-be child process's
     * output buffer.
     */
    screen_flush();

    /*
     * Get the connection command.
     */
    command = connect_command();

    /*
     * Spawn and go.
     */
    spawn_process(command, q_status.emulation);

    /*
     * Reset the bytes connection bytes counter.
     */
    q_connection_bytes_received = 0;

    /*
     * The connection is already open and ready to go.  We need to make it
     * non-blocking.
     */
    set_nonblock(q_child_tty_fd);

    /*
     * Parent process is online.
     */
    time(&q_status.connect_time);
    q_status.online = Q_TRUE;

    /*
     * Switch to doorway mode if requested.
     */
    if (strcasecmp(get_option(Q_OPTION_CONNECT_DOORWAY), "doorway") == 0) {
        q_status.doorway_mode = Q_DOORWAY_MODE_FULL;
    } else if (strcasecmp(get_option(Q_OPTION_CONNECT_DOORWAY), "mixed") == 0) {
        q_status.doorway_mode = Q_DOORWAY_MODE_MIXED;
    } else {
        q_status.doorway_mode = Q_DOORWAY_MODE_OFF;
    }

    /*
     * Non-network connections: go immediately to console.
     */
    switch_state(Q_STATE_CONSOLE);
}
