/*
 * shell.h
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef __SHELL_H__
#define __SHELL_H__

/* Includes --------------------------------------------------------------- */

#ifdef __cplusplus
extern "C" {
#endif

/* Defines ---------------------------------------------------------------- */

/* Globals ---------------------------------------------------------------- */

/* Functions -------------------------------------------------------------- */

/**
 * Set a tty into raw mode.
 *
 * @param tty_fd the tty descriptor
 */
extern void set_raw_termios(const int tty_fd);

/**
 * Spawn a local shell in its own pty.
 */
extern void launch_shell();

#ifdef __cplusplus
}
#endif

#endif /* __SHELL_H__ */
