/*
 * states.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "qcurses.h"
#include "common.h"

#include <assert.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "console.h"
#include "protocols.h"
#include "keyboard.h"
#include "screen.h"
#include "options.h"
#include "states.h"

/**
 * Global state.
 */
Q_PROGRAM_STATE q_program_state;

/**
 * Whether or not the keyboard is supposed to be blocking (last argument to
 * nodelay()).
 */
Q_BOOL q_keyboard_blocks;

/**
 * Look for input from the keyboard and mouse.  If input came in, dispatch it
 * to the appropriate keyboard handler for the current program state.
 */
void keyboard_handler() {

    int flags = 0;              /* passed to *_keyboard_handler() */
    int keystroke;              /* " */

    /*
     * Grab keystroke
     */
    if (q_keyboard_blocks == Q_TRUE) {
        qodem_getch(&keystroke, &flags, Q_KEYBOARD_DELAY);
    } else {
        qodem_getch(&keystroke, &flags, 0);
    }

    if ((keystroke == Q_ERR) && (q_status.passthru == Q_FALSE)) {
        /*
         * No data, return
         */
        return;
    }

    switch (q_program_state) {

    case Q_STATE_CONSOLE:
        console_keyboard_handler(keystroke, flags);
        break;
    case Q_STATE_CONSOLE_MENU:
        console_menu_keyboard_handler(keystroke, flags);
        break;
    case Q_STATE_SCROLLBACK:
        scrollback_keyboard_handler(keystroke, flags);
        break;

#ifndef Q_NO_PROTOCOLS
    case Q_STATE_DOWNLOAD_MENU:
    case Q_STATE_UPLOAD_MENU:
        protocol_menu_keyboard_handler(keystroke, flags);
        break;
    case Q_STATE_DOWNLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_BATCH_DIALOG:
        protocol_pathdialog_keyboard_handler(keystroke, flags);
        break;
    case Q_STATE_UPLOAD:
    case Q_STATE_UPLOAD_BATCH:
    case Q_STATE_DOWNLOAD:
        protocol_transfer_keyboard_handler(keystroke, flags);
        break;
#endif

    case Q_STATE_EMULATION_MENU:
        emulation_menu_keyboard_handler(keystroke, flags);
        break;

#ifndef Q_NO_KEYMACROS
    case Q_STATE_FUNCTION_KEY_EDITOR:
        function_key_editor_keyboard_handler(keystroke, flags);
        break;
#endif

    case Q_STATE_CODEPAGE:
        codepage_keyboard_handler(keystroke, flags);
        break;

    case Q_STATE_INITIALIZATION:
    case Q_STATE_EXIT:
        /*
         * Program BUG
         */
        abort();
    }
}

/**
 * Dispatch to the appropriate draw function for the current program state.
 */
void refresh_handler() {

    static suseconds_t last_time = 1000000;
    struct timeval tv;

    switch (q_program_state) {

    case Q_STATE_CONSOLE:
        /*
         * Only update the console 16 times a second when in flood.
         */
        if (q_console_flood == Q_TRUE) {
            gettimeofday(&tv, NULL);
            if ((tv.tv_usec < last_time) || (tv.tv_usec - last_time > 62500)) {
                console_refresh(Q_TRUE);
                last_time = tv.tv_usec;
            }
        } else {
            console_refresh(Q_TRUE);
        }
        break;
    case Q_STATE_CONSOLE_MENU:
        console_menu_refresh();
        break;
    case Q_STATE_SCROLLBACK:
        scrollback_refresh();
        break;

#ifndef Q_NO_PROTOCOLS
    case Q_STATE_DOWNLOAD_MENU:
    case Q_STATE_UPLOAD_MENU:
        protocol_menu_refresh();
        break;
    case Q_STATE_DOWNLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_BATCH_DIALOG:
        protocol_pathdialog_refresh();
        break;
    case Q_STATE_UPLOAD:
    case Q_STATE_UPLOAD_BATCH:
    case Q_STATE_DOWNLOAD:
        protocol_transfer_refresh();
        break;
#endif

    case Q_STATE_EMULATION_MENU:
        emulation_menu_refresh();
        break;

#ifndef Q_NO_KEYMACROS
    case Q_STATE_FUNCTION_KEY_EDITOR:
        function_key_editor_refresh();
        break;
#endif

    case Q_STATE_CODEPAGE:
        codepage_refresh();
        break;

    case Q_STATE_EXIT:
        /*
         * Someone called refresh, but we are on the way out.
         */
        break;

    case Q_STATE_INITIALIZATION:
        /*
         * Program BUG
         */
        abort();

    }
}

/**
 * Switch to a new state, handling things like visible cursor, blocking
 * keyboard, etc.
 *
 * @param new_state state to switch to
 */
void switch_state(const Q_PROGRAM_STATE new_state) {

    /*
     * Passthru: when going to console, set the state, but kill ncurses.
     */
    if ((q_status.passthru == Q_TRUE) && (new_state == Q_STATE_CONSOLE)) {
        screen_teardown();
        q_program_state = new_state;

        /*
         * Explicitly check for the mouse reporting flag.  Doing this
         * here permits GPM support for Linux console.
         */
        if ((q_status.xterm_mouse_reporting == Q_TRUE) &&
            ((q_status.emulation == Q_EMUL_XTERM) ||
                (q_status.emulation == Q_EMUL_XTERM_UTF8))
        ) {
            /*
             * xterm emulations: listen for the mouse.
             */
            enable_mouse_listener();
        } else {
            /*
             * Non-xterm or mouse disabled, do not listen for the mouse.
             */
            disable_mouse_listener();
        }
        return;
    } else if (q_status.passthru == Q_TRUE) {
        /*
         * Going to a non-console state, bring up curses.
         */
        screen_setup();
        initialize_keyboard();
    }

    if ((q_program_state == Q_STATE_CONSOLE) &&
        (has_true_doublewidth() == Q_TRUE)) {

        screen_clear();
    }

    switch (new_state) {

#ifndef Q_NO_PROTOCOLS
    case Q_STATE_DOWNLOAD_MENU:
    case Q_STATE_UPLOAD_MENU:
    case Q_STATE_DOWNLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_PATHDIALOG:
    case Q_STATE_UPLOAD_BATCH_DIALOG:
#endif
    case Q_STATE_EMULATION_MENU:
    case Q_STATE_INITIALIZATION:
    case Q_STATE_CODEPAGE:
        set_blocking_input(Q_TRUE);
        q_keyboard_blocks = Q_TRUE;
        q_screen_dirty = Q_TRUE;
        q_cursor_on();
        break;

    case Q_STATE_SCROLLBACK:
        if (q_scrollback_search_string != NULL) {
            Xfree(q_scrollback_search_string, __FILE__, __LINE__);
            q_scrollback_search_string = NULL;
        }
        q_scrollback_highlight_search_string = Q_FALSE;
        /*
         * Fall through...
         */
#ifndef Q_NO_PROTOCOLS
    case Q_STATE_UPLOAD:
    case Q_STATE_UPLOAD_BATCH:
    case Q_STATE_DOWNLOAD:
#endif
    case Q_STATE_CONSOLE_MENU:
        set_blocking_input(Q_FALSE);
        q_keyboard_blocks = Q_FALSE;
        q_screen_dirty = Q_TRUE;
        q_cursor_off();
        break;

    case Q_STATE_CONSOLE:
        set_blocking_input(Q_FALSE);
        q_keyboard_blocks = Q_FALSE;
        screen_clear();
        q_screen_dirty = Q_TRUE;
        if (q_status.split_screen == Q_TRUE) {
            q_split_screen_dirty = Q_TRUE;
        }
        if (((q_status.emulation == Q_EMUL_LINUX)
             || (q_status.emulation == Q_EMUL_LINUX_UTF8)
             || (q_status.emulation == Q_EMUL_VT220))
            && (q_status.visible_cursor == Q_FALSE)) {
            q_cursor_off();
        } else {
            q_cursor_on();
        }
        break;

#ifndef Q_NO_KEYMACROS
    case Q_STATE_FUNCTION_KEY_EDITOR:
        /*
         * Fall through ...
         */
        set_blocking_input(Q_TRUE);
        q_keyboard_blocks = Q_TRUE;
        q_screen_dirty = Q_TRUE;
        q_cursor_off();
        break;
#endif

    case Q_STATE_EXIT:
        q_cursor_on();
        break;

    }

    q_program_state = new_state;
}
