/*
 * states.h
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef __STATES_H__
#define __STATES_H__

/* Includes --------------------------------------------------------------- */

#ifdef __cplusplus
extern "C" {
#endif

/* Defines ---------------------------------------------------------------- */

/**
 * Program states.  First state is always initialization, from there it can
 * be dialing directory or console.
 */
typedef enum Q_PROGRAM_STATES {
    Q_STATE_INITIALIZATION,     /* Initialization */

    /**
     * TERMINAL mode.  See console.c.
     */
    Q_STATE_CONSOLE,            /* Console */
    Q_STATE_CONSOLE_MENU,       /* Console menu */
    Q_STATE_SCROLLBACK,         /* Console scrollback */

#ifndef Q_NO_PROTOCOLS

    /**
     * Uploads and downloads.  See protocols.c.
     */
    Q_STATE_DOWNLOAD,                   /* Downloading file */
    Q_STATE_DOWNLOAD_MENU,              /* Download file menu */
    Q_STATE_DOWNLOAD_PATHDIALOG,        /* Download file/path dialog */
    Q_STATE_UPLOAD,                     /* Uploading file */
    Q_STATE_UPLOAD_BATCH,               /* Uploading many files */
    Q_STATE_UPLOAD_BATCH_DIALOG,        /* Uploading many files dialog */
    Q_STATE_UPLOAD_MENU,                /* Upload file menu */
    Q_STATE_UPLOAD_PATHDIALOG,          /* Upload file/path dialog */

#endif /* Q_NO_PROTOCOLS */

    /**
     * Emulations.  See emulation.c.
     */
    Q_STATE_EMULATION_MENU,     /* Emulation select menu */

#ifndef Q_NO_KEYMACROS

    /**
     * Function key editor.  See keyboard.c.
     */
    Q_STATE_FUNCTION_KEY_EDITOR,        /* Function key editor */

#endif /* Q_NO_KEYMACROS */

    /**
     * Codepage dialog.  See codepage.c.
     */
    Q_STATE_CODEPAGE,           /* Codepage dialog */

    Q_STATE_EXIT                /* Exit program */
} Q_PROGRAM_STATE;

/* Globals ---------------------------------------------------------------- */

/**
 * Global state.
 */
extern Q_PROGRAM_STATE q_program_state;

/**
 * Whether or not the keyboard is supposed to be blocking (last argument to
 * nodelay()).
 */
extern Q_BOOL q_keyboard_blocks;

/* Functions -------------------------------------------------------------- */

/**
 * Switch to a new state, handling things like visible cursor, blocking
 * keyboard, etc.
 *
 * @param new_state state to switch to
 */
extern void switch_state(const Q_PROGRAM_STATE new_state);

/**
 * Look for input from the keyboard and mouse.  If input came in, dispatch it
 * to the appropriate keyboard handler for the current program state.
 */
extern void keyboard_handler();

/**
 * Dispatch to the appropriate draw function for the current program state.
 */
extern void refresh_handler();

#ifdef __cplusplus
}
#endif

#endif /* __STATES_H__ */
