/*
 * colors.c
 *
 * lcxterm - Linux Console X-like Terminal
 *
 * Written 2003-2021 by Autumn Lamonte ⚧ Trans Liberation Now
 *
 * To the extent possible under law, the author(s) have dedicated all
 * copyright and related and neighboring rights to this software to the
 * public domain worldwide. This software is distributed without any
 * warranty.
 *
 * You should have received a copy of the CC0 Public Domain Dedication along
 * with this software. If not, see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "qcurses.h"
#include "common.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "main.h"
#include "options.h"
#include "colors.h"

#ifdef __clang__
/*
 * Disable the UTF-8 encoding check for this file.  We use TheDraw to make
 * the info screen, which saves in raw 8-bit bytes.
 */
#pragma clang diagnostic ignored "-Winvalid-source-encoding"
#endif

/**
 * The color pair number that is white foreground black background.
 *
 * On the raw Linux console, this will be 0.  But most X11-based terminal
 * emulators support lots of colors, so I can define my own white-on-black
 * color pair number; this is handy to always really get white-on-black for
 * terminals that have a different default color.
 */
short q_white_color_pair_num = 0;

/**
 * The offset between normal and bolded colors.  screen.c needs to peek at
 * this, so it's not static.  By default set it such that A_BOLD will not be
 * sent directly to the screen, using instead colors that match the DOS CGA
 * colors (e.g. bold brown = yellow).
 */
short q_color_bold_offset = 64;

/**
 * Global colormap table.
 */
struct q_text_color_struct q_text_colors[Q_COLOR_MAX];

/**
 * Convert a color string to a curses color number.
 *
 * @param original the color to return if new_color does not specify a valid
 * color string
 * @param new_color the color string.  The valid color names are "blue",
 * "red", etc., with "yellow" and "brown" being synonyms.
 * @return the color specified by new_color, or original if this was an
 * unknown string
 */
static short color_from_string(short original, char * new_color) {
    if (strncasecmp(new_color, "blue", strlen("blue")) == 0) {
        return COLOR_BLUE;
    }
    if (strncasecmp(new_color, "white", strlen("white")) == 0) {
        return COLOR_WHITE;
    }
    if (strncasecmp(new_color, "black", strlen("black")) == 0) {
        return COLOR_BLACK;
    }
    if (strncasecmp(new_color, "red", strlen("red")) == 0) {
        return COLOR_RED;
    }
    if (strncasecmp(new_color, "green", strlen("green")) == 0) {
        return COLOR_GREEN;
    }
    if (strncasecmp(new_color, "yellow", strlen("yellow")) == 0) {
        return COLOR_YELLOW;
    }
    if (strncasecmp(new_color, "cyan", strlen("cyan")) == 0) {
        return COLOR_CYAN;
    }
    if (strncasecmp(new_color, "magenta", strlen("magenta")) == 0) {
        return COLOR_MAGENTA;
    }

    /*
     * Synonyms
     */
    if (strncasecmp(new_color, "brown", strlen("brown")) == 0) {
        return COLOR_YELLOW;
    }
    if (strncasecmp(new_color, "grey", strlen("grey")) == 0) {
        return COLOR_WHITE;
    }
    if (strncasecmp(new_color, "gray", strlen("gray")) == 0) {
        return COLOR_WHITE;
    }

    return original;
}

/**
 * Set a text color from a line in the configuration file.  Each line has the
 * form <foreground>, <background> [, bold ]
 *
 * @param color the UI element color
 * @param line the line from the config file
 */
static void set_q_color(struct q_text_color_struct * color, const char * line) {
    char * begin;
    char * end;

    begin = (char *) line;
    end = strchr(begin, ',');
    if (end == NULL) {
        return;
    }

    /*
     * Default no bold
     */
    color->bold = Q_FALSE;

    *end = 0;
    color->fg = color_from_string(color->fg, begin);
    begin = end + 1;
    while ((*begin != 0) && (q_isspace(*begin))) {
        begin++;
    }
    if (*begin == 0) {
        return;
    }

    end = strchr(begin, ',');
    if (end == NULL) {
        color->bg = color_from_string(color->bg, begin);
        return;
    }
    *end = 0;
    color->bg = color_from_string(color->bg, begin);
    begin = end + 1;
    while ((*begin != 0) && (q_isspace(*begin))) {
        begin++;
    }
    if (*begin == 0) {
        return;
    }
    if (strncasecmp(begin, "bold", strlen("bold")) == 0) {
        color->bold = Q_TRUE;
    }
}

/**
 * This must be called to initialize the colors from the config file.
 */
void load_colors() {
    const short COLOR_DEFAULT = COLOR_BLACK;

    q_text_colors[Q_COLOR_STATUS].bold = Q_FALSE;
    q_text_colors[Q_COLOR_STATUS].fg = COLOR_BLUE;
    q_text_colors[Q_COLOR_STATUS].bg = COLOR_WHITE;

    q_text_colors[Q_COLOR_STATUS_DISABLED].bold = Q_TRUE;
    q_text_colors[Q_COLOR_STATUS_DISABLED].fg = COLOR_BLACK;
    q_text_colors[Q_COLOR_STATUS_DISABLED].bg = COLOR_WHITE;

    q_text_colors[Q_COLOR_CONSOLE_TEXT].bold = Q_FALSE;
    q_text_colors[Q_COLOR_CONSOLE_TEXT].fg = COLOR_WHITE;
    q_text_colors[Q_COLOR_CONSOLE_TEXT].bg = COLOR_DEFAULT;

    q_text_colors[Q_COLOR_CONSOLE_BACKGROUND].bold = Q_FALSE;
    q_text_colors[Q_COLOR_CONSOLE_BACKGROUND].fg = COLOR_WHITE;
    q_text_colors[Q_COLOR_CONSOLE_BACKGROUND].bg = COLOR_BLACK;

    q_text_colors[Q_COLOR_WINDOW_BORDER].bold = Q_TRUE;
    q_text_colors[Q_COLOR_WINDOW_BORDER].fg = COLOR_BLUE;
    q_text_colors[Q_COLOR_WINDOW_BORDER].bg = COLOR_BLACK;

    q_text_colors[Q_COLOR_WINDOW].bold = Q_FALSE;
    q_text_colors[Q_COLOR_WINDOW].fg = COLOR_DEFAULT;
    q_text_colors[Q_COLOR_WINDOW].bg = COLOR_BLUE;

    q_text_colors[Q_COLOR_MENU_COMMAND].bold = Q_TRUE;
    q_text_colors[Q_COLOR_MENU_COMMAND].fg = COLOR_YELLOW;
    q_text_colors[Q_COLOR_MENU_COMMAND].bg = COLOR_BLUE;

    q_text_colors[Q_COLOR_MENU_COMMAND_UNAVAILABLE].bold = Q_TRUE;
    q_text_colors[Q_COLOR_MENU_COMMAND_UNAVAILABLE].fg = COLOR_BLACK;
    q_text_colors[Q_COLOR_MENU_COMMAND_UNAVAILABLE].bg = COLOR_BLUE;

    q_text_colors[Q_COLOR_MENU_TEXT].bold = Q_FALSE;
    q_text_colors[Q_COLOR_MENU_TEXT].fg = COLOR_WHITE;
    q_text_colors[Q_COLOR_MENU_TEXT].bg = COLOR_BLUE;

    q_text_colors[Q_COLOR_WINDOW_FIELD_HIGHLIGHTED].bold = Q_FALSE;
    q_text_colors[Q_COLOR_WINDOW_FIELD_HIGHLIGHTED].fg = COLOR_WHITE;
    q_text_colors[Q_COLOR_WINDOW_FIELD_HIGHLIGHTED].bg = COLOR_BLUE;

    q_text_colors[Q_COLOR_WINDOW_FIELD_TEXT_HIGHLIGHTED].bold = Q_TRUE;
    q_text_colors[Q_COLOR_WINDOW_FIELD_TEXT_HIGHLIGHTED].fg = COLOR_YELLOW;
    q_text_colors[Q_COLOR_WINDOW_FIELD_TEXT_HIGHLIGHTED].bg = COLOR_BLUE;

    q_text_colors[Q_COLOR_LIST_SELECTED].bold = Q_FALSE;
    q_text_colors[Q_COLOR_LIST_SELECTED].fg = COLOR_WHITE;
    q_text_colors[Q_COLOR_LIST_SELECTED].bg = COLOR_BLACK;

    /* Now that defaults are set, load the console text color. */
    set_q_color(&q_text_colors[Q_COLOR_CONSOLE_TEXT],
        get_option(Q_OPTION_DEFAULT_TEXT_COLOR));
    q_text_colors[Q_COLOR_CONSOLE_BACKGROUND].bg =
        q_text_colors[Q_COLOR_CONSOLE_TEXT].bg;

}

/**
 * Set up curses colors.
 */
void q_setup_curses_colors() {
    short i;

    /*
     * Initialize the 64 curses colors.
     */
    if ((COLORS >= 16) && (COLOR_PAIRS >= 2 * q_color_bold_offset) &&
        (can_change_color() == TRUE)
    ) {
        /*
         * Complete re-map both the colors and color pairs.  Note that the
         * max color value is 1000.  These values are gamma-corrected.  Some
         * other potential values:
         *
         *  gamma  |  333  |  666  | 999
         * ------------------------------
         *    1.0  |  333  |  666  | 999
         *    1.2  |  267  |  614  | 999
         *    1.4  |  215  |  568  | 999
         *    1.6  |  172  |  522  | 999
         *    1.8  |  138  |  481  | 999
         *    2.0  |  111  |  444  | 999
         *    2.2  |   89  |  409  | 999
         *    2.4  |   71  |  377  | 999
         */
#define GAMMA_000 000
#define GAMMA_333 267
#define GAMMA_666 614
#define GAMMA_999 999

        /*
         * Normal intensity colors
         */
        init_color(COLOR_BLACK,   GAMMA_000, GAMMA_000, GAMMA_000);
        init_color(COLOR_RED,     GAMMA_666, GAMMA_000, GAMMA_000);
        init_color(COLOR_GREEN,   GAMMA_000, GAMMA_666, GAMMA_000);
        init_color(COLOR_YELLOW,  GAMMA_666, GAMMA_333, GAMMA_000);
        init_color(COLOR_BLUE,    GAMMA_000, GAMMA_000, GAMMA_666);
        init_color(COLOR_MAGENTA, GAMMA_666, GAMMA_000, GAMMA_666);
        init_color(COLOR_CYAN,    GAMMA_000, GAMMA_666, GAMMA_666);
        init_color(COLOR_WHITE,   GAMMA_666, GAMMA_666, GAMMA_666);

        /*
         * Bright intensity colors
         */
        init_color(8 + COLOR_BLACK,   GAMMA_333, GAMMA_333, GAMMA_333);
        init_color(8 + COLOR_RED,     GAMMA_999, GAMMA_333, GAMMA_333);
        init_color(8 + COLOR_GREEN,   GAMMA_333, GAMMA_999, GAMMA_333);
        init_color(8 + COLOR_YELLOW,  GAMMA_999, GAMMA_999, GAMMA_333);
        init_color(8 + COLOR_BLUE,    GAMMA_333, GAMMA_333, GAMMA_999);
        init_color(8 + COLOR_MAGENTA, GAMMA_999, GAMMA_333, GAMMA_999);
        init_color(8 + COLOR_CYAN,    GAMMA_333, GAMMA_999, GAMMA_999);
        init_color(8 + COLOR_WHITE,   GAMMA_999, GAMMA_999, GAMMA_999);

        /*
         * Now init the pairs
         */

        /*
         * Normal intensity
         */
        for (i = 1; i < q_color_bold_offset; i++) {
            init_pair(i, (short) ((i & 0x38) >> 3), (short) (i & 0x07));
        }

        /*
         * Bright intensity
         */
        for (i = 1 + q_color_bold_offset; i < 2 * q_color_bold_offset; i++) {
            init_pair(i,
                      (short) ((((i - q_color_bold_offset) & 0x38) >> 3) + 8),
                      (short) ((i - q_color_bold_offset) & 0x07));
        }

        /*
         * Special case: put black-on-black at 0x38 and 64 + 0x38
         */
        init_pair(0x38                      , COLOR_BLACK    , COLOR_BLACK);
        init_pair(0x38 + q_color_bold_offset, COLOR_BLACK + 8, COLOR_BLACK);

        /*
         * Special case: put white-on-black at 64 and 64 + 64
         */
        init_pair(64                      , COLOR_WHITE    , COLOR_BLACK);
        init_pair(64 + q_color_bold_offset, COLOR_WHITE + 8, COLOR_BLACK);

        q_white_color_pair_num = 64;

        /*
         * Bail out here.
         */
        return;
    }

    for (i = 1; (i < 64) && (i < COLOR_PAIRS); i++) {
        init_pair(i, (short) ((i & 0x38) >> 3), (short) (i & 0x07));
    }

    /*
     * Special case: put black-on-black at 0x38
     */
    init_pair(0x38, 0x00, 0x00);

    /*
     * We will send A_BOLD directly to the screen.
     */
    q_color_bold_offset = 0;

    if (COLOR_PAIRS > 64) {
        /*
         * Make my own white-on-black color
         */
        init_pair(64, COLOR_WHITE, COLOR_BLACK);
        q_white_color_pair_num = 64;
    } else {
        /*
         * Assume color pair 0 is white on black
         */
        assume_default_colors(COLOR_WHITE, COLOR_BLACK);
        q_white_color_pair_num = 0;
    }
}

/**
 * Use up to 256 characters for the HTML/CSS color tag.
 */
static char font_color[256];

#ifdef Q_ENABLE_24BITRGB

/**
 * The RGB color tag.
 */
static char rgb_font_color[12];

/**
 * Convert a direct RGB into HTML color string.  Note that the string
 * returned is a single static buffer, i.e. this is NOT thread-safe.
 *
 * @param rgb the 24-bit RGB color
 * @return the HTML string
 */
static char * rgb_to_html(const int32_t rgb) {
    sprintf(rgb_font_color, "#%06x", rgb);
    return rgb_font_color;
}

/**
 * Convert a curses attr_t or a direct RGB into an HTML &lt;font color&gt;
 * tag.  Note that the string returned is a single static buffer, i.e. this
 * is NOT thread-safe.
 *
 * @param attr the curses attribute
 * @param fg_rgb the foreground 24-bit RGB color, or -1 if not set
 * @param bg_rgb the background 24-bit RGB color, or -1 if not set
 * @return the HTML string
 */
char * color_to_html(const attr_t attr, const int32_t fg_rgb,
    const int32_t bg_rgb) {

    char * font_weight = "normal";
    char * text_decoration = "none";
    char * fg_text;
    char * bg_text;

    char * bitmask_to_html_map[] = {
        "#000000",              /* COLOR_BLACK   */
        "#AB0000",              /* COLOR_RED     */
        "#00AB00",              /* COLOR_GREEN   */
        "#996600",              /* COLOR_YELLOW  */
        "#0000AB",              /* COLOR_BLUE    */
        "#990099",              /* COLOR_MAGENTA */
        "#009999",              /* COLOR_CYAN    */
        "#ABABAB",              /* COLOR_WHITE   */
    };

    char * bitmask_to_html_map_bright[] = {
        "#545454",              /* COLOR_BLACK   */
        "#FF6666",              /* COLOR_RED     */
        "#66FF66",              /* COLOR_GREEN   */
        "#FFFF66",              /* COLOR_YELLOW  */
        "#6666FF",              /* COLOR_BLUE    */
        "#FF66FF",              /* COLOR_MAGENTA */
        "#66FFFF",              /* COLOR_CYAN    */
        "#FFFFFF",              /* COLOR_WHITE   */
    };

    short fg;
    short bg;

    fg = (short) (PAIR_NUMBER(attr) >> 3);
    bg = (short) (PAIR_NUMBER(attr) & 0x07);

    memset(font_color, 0, sizeof(font_color));

    if ((attr & A_BLINK) && (attr & A_UNDERLINE)) {
        text_decoration = "blink, underline";
    } else if (attr & A_UNDERLINE) {
        text_decoration = "underline";
    } else if (attr & A_BLINK) {
        text_decoration = "blink";
    }
    if (attr & A_REVERSE) {
        if (bg_rgb == -1) {
            fg_text = bitmask_to_html_map[bg];
        } else {
            fg_text = rgb_to_html(bg_rgb);
        }
        if (attr & A_BOLD) {
            if (fg_rgb == -1) {
                bg_text = bitmask_to_html_map_bright[fg];
            } else {
                bg_text = rgb_to_html(fg_rgb);
            }
        } else {
            if (fg_rgb == -1) {
                bg_text = bitmask_to_html_map[fg];
            } else {
                bg_text = rgb_to_html(fg_rgb);
            }
        }
    } else {
        if (bg_rgb == -1) {
            bg_text = bitmask_to_html_map[bg];
        } else {
            fg_text = rgb_to_html(bg_rgb);
        }
        if (attr & A_BOLD) {
            if (fg_rgb == -1) {
                fg_text = bitmask_to_html_map_bright[fg];
            } else {
                fg_text = rgb_to_html(fg_rgb);
            }
        } else {
            if (fg_rgb == -1) {
                fg_text = bitmask_to_html_map[fg];
            } else {
                fg_text = rgb_to_html(fg_rgb);
            }
        }
    }

    sprintf(font_color, "style=\"color: %s; background-color: %s; " \
        "text-decoration: %s; font-weight: %s\"",
        fg_text, bg_text, text_decoration, font_weight);

    return font_color;
}

#else

/**
 * Convert a curses attr_t into an HTML &lt;font color&gt; tag.  Note that
 * the string returned is a single static buffer, i.e. this is NOT
 * thread-safe.
 *
 * @param attr the curses attribute
 * @return the HTML string
 */
char * color_to_html(const attr_t attr) {

    char * font_weight = "normal";
    char * text_decoration = "none";
    char * fg_text;
    char * bg_text;

    char * bitmask_to_html_map[] = {
        "#000000",              /* COLOR_BLACK   */
        "#AB0000",              /* COLOR_RED     */
        "#00AB00",              /* COLOR_GREEN   */
        "#996600",              /* COLOR_YELLOW  */
        "#0000AB",              /* COLOR_BLUE    */
        "#990099",              /* COLOR_MAGENTA */
        "#009999",              /* COLOR_CYAN    */
        "#ABABAB",              /* COLOR_WHITE   */
    };

    char * bitmask_to_html_map_bright[] = {
        "#545454",              /* COLOR_BLACK   */
        "#FF6666",              /* COLOR_RED     */
        "#66FF66",              /* COLOR_GREEN   */
        "#FFFF66",              /* COLOR_YELLOW  */
        "#6666FF",              /* COLOR_BLUE    */
        "#FF66FF",              /* COLOR_MAGENTA */
        "#66FFFF",              /* COLOR_CYAN    */
        "#FFFFFF",              /* COLOR_WHITE   */
    };

    short fg;
    short bg;

    fg = (short) (PAIR_NUMBER(attr) >> 3);
    bg = (short) (PAIR_NUMBER(attr) & 0x07);

    memset(font_color, 0, sizeof(font_color));

    if ((attr & A_BLINK) && (attr & A_UNDERLINE)) {
        text_decoration = "blink, underline";
    } else if (attr & A_UNDERLINE) {
        text_decoration = "underline";
    } else if (attr & A_BLINK) {
        text_decoration = "blink";
    }
    if (attr & A_REVERSE) {
        fg_text = bitmask_to_html_map[bg];
        if (attr & A_BOLD) {
            bg_text = bitmask_to_html_map_bright[fg];
        } else {
            bg_text = bitmask_to_html_map[fg];
        }
    } else {
        bg_text = bitmask_to_html_map[bg];
        if (attr & A_BOLD) {
            fg_text = bitmask_to_html_map_bright[fg];
        } else {
            fg_text = bitmask_to_html_map[fg];
        }
    }

    sprintf(font_color, "style=\"color: %s; background-color: %s; " \
        "text-decoration: %s; font-weight: %s\"",
        fg_text, bg_text, text_decoration, font_weight);

    return font_color;
}

#endif /* Q_ENABLE_24BITRGB */
